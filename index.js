import yargs from 'yargs';
import chalk from 'chalk';
import {baseUrl} from './baseUrl.js'
import {appendFileSync, readFileSync} from 'fs'
import clipboardy from 'clipboardy'
import validator from 'validator'
import emoji from 'node-emoji'

yargs.command('add', 'ajouter un lien.', {
    page: {
        describe: 'add page',
        demandOption: true,
        type: 'string'
    },
    source:{
        describe: 'add source',
        demandOption: true,
        type: 'string'
    },
    medium:{
        describe: 'add medium',
        type: 'string'
    },   
    campaign:{
        describe: 'add campaign',
        type: 'string'
    },

}, argv => {
    const {page, source, medium, campaign} = argv
    const link = `${baseUrl}${page}?utm_source=${source}&utm_medium=${medium}&utm_campaign=${campaign}`


    if(!validator.isURL(link)){
        console.log(chalk.red.bold('\nLe lien est incorrecte'))
        return
    }
    appendFileSync('links.txt', `${link}\n`)

    clipboardy.writeSync(link)

    console.log(chalk.inverse.bold('\nLien copié!'))


    console.log(chalk.green.bold`${link}\n`)
}
).argv

yargs.command('list', 'lire les liens',() =>{
    const links = readFileSync('links.txt').toString().split(`\n`)
    for (const i in links) {
        console.log(`${emoji.get('star')} ${links[i]}`) 
        }
}).argv
